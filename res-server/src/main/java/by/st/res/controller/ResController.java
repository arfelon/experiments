package by.st.res.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1")
public class ResController {

    @GetMapping(value = "/item", produces = MediaType.TEXT_PLAIN_VALUE)
    public String getItem(Principal principal) {
        return "Hi " + principal.getName();
    }
}
