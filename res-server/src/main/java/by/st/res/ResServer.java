package by.st.res;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResServer {

    public static void main(String[] args) {
        SpringApplication.run(ResServer.class, args);
    }
}
