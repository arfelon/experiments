--
insert into oauth_client_details (
    client_id,
    client_secret,
    scope,
    authorized_grant_types,
    access_token_validity,
    refresh_token_validity
) values (
    'client',
    '{noop}secret',
    'web,mob',
    'refresh_token,password',
    3600,
    14400
);

--
insert into users (username, password, enabled) values ('user1', '{noop}password', true);
insert into authorities (username, authority) values ('user1', 'ROLE_USER');

insert into users (username, password, enabled) values ('user2', '{noop}password', true);
insert into authorities (username, authority) values ('user2', 'ROLE_USER');
