--
drop table oauth_client_details if exists;
create table oauth_client_details (
    client_id varchar(256) primary key,
    resource_ids varchar(256),
    client_secret varchar(256),
    scope varchar(256),
    authorized_grant_types varchar(256),
    web_server_redirect_uri varchar(256),
    authorities varchar(256),
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information varchar(4096),
    autoapprove varchar(256)
);

drop table oauth_access_token if exists;
create table oauth_access_token (
    token_id varchar(256),
    token longvarbinary,
    authentication_id varchar(256) primary key,
    user_name varchar(256),
    client_id varchar(256),
    authentication longvarbinary,
    refresh_token varchar(256)
);

drop table oauth_refresh_token if exists;
create table oauth_refresh_token (
    token_id varchar(256),
    token longvarbinary,
    authentication longvarbinary
);

--
drop table users if exists;
create table users(
    username varchar(256) not null primary key,
    password varchar(256) not null,
    enabled boolean not null
);

drop table authorities if exists;
create table authorities (
    username varchar(256) not null,
    authority varchar(256) not null,
    constraint fk_authorities_users foreign key(username) references users(username)
);
create unique index ix_auth_username on authorities (username, authority);
