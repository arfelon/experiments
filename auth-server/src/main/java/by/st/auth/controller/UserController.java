package by.st.auth.controller;

import org.springframework.http.MediaType;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

    @GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Map<String, Object> getUser(OAuth2Authentication auth) {
        UserDetails user = (UserDetails) auth.getUserAuthentication().getPrincipal();

        Map<String, Object> params = new HashMap<>();
        params.put("username", user.getUsername());
        params.put("authorities", AuthorityUtils.authorityListToSet(user.getAuthorities()));

        return params;
    }
}
